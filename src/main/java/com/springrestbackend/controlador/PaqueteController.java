

package com.springrestbackend.controlador;

import com.springrestbackend.Modelo.Aeropuerto;
import com.springrestbackend.Modelo.EstadoPaqueteName;
import com.springrestbackend.Modelo.Paquete;
import com.springrestbackend.Modelo.Usuario;
import com.springrestbackend.algorithm.AntColony;
import com.springrestbackend.algorithm.CSVReader;
import com.springrestbackend.repo.AeropuertoRepository;
import com.springrestbackend.repo.PaqueteRepository;
import com.springrestbackend.repo.UsuarioRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

//@CrossOrigin(origins = "localhost:8080")
@RestController
@RequestMapping("/api")
public class PaqueteController {
    @Autowired
    PaqueteRepository paqueteRepository;

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    AeropuertoRepository aeropuertoRepository;

    @PostMapping("/paquete")
    public Paquete postPaquete(@RequestBody Paquete paquete) {
        String orig = paquete.getAerOrigen().getCodAeropuerto();
        String codtracking = "000000000";

        Long longitud = paqueteRepository.findCount();
        if (longitud == 0)
            codtracking = "000000001";
        else{

            Long idmax = paqueteRepository.findMax() + 1;
            String codmax = idmax.toString();
            codtracking = codtracking + codmax;
        }
        codtracking = codtracking.substring(codtracking.length()-9);
        codtracking = orig + codtracking;
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        paquete.setFechaPedido(timestamp);
        Paquete _paquete = paqueteRepository.save(new Paquete(paquete.getUserOrigen(), paquete.getUserDestino(),
                                                              paquete.getAerOrigen(), paquete.getAerDestino(),
                                                              codtracking, paquete.getFechaPedido()));//,
                                                              //paquete.getFechaDestino()));
        return _paquete;
    }

    @GetMapping("/gestionarpaquetes")
    public List<Paquete> getAllPaquetes() {
        List<Paquete> paquetes = new ArrayList<>();
        paqueteRepository.findAll().forEach(paquetes::add);
        return paquetes;
    }


    @GetMapping("/gestionarpaquetes/{id}")
    public Optional<Paquete> getPaqueteById(@PathVariable("id") Long id) {

        Optional<Paquete> paquete = paqueteRepository.findById(id);
        return paquete;
    }

    @PutMapping("/gestionarpaquetes/{id}")
    public ResponseEntity<Paquete> updatePaquete(@PathVariable("id") Long id, @RequestBody Paquete paquete) {
        Optional<Paquete> paquetedata = paqueteRepository.findById(id);
        if(paquetedata.isPresent() && paquetedata.get().getEstadoPaquete()== EstadoPaqueteName.REGISTRADO) {
            Paquete _paquete = paquetedata.get();
            _paquete.setEstadoPaquete(paquete.getEstadoPaquete());
            String asunto = "CAMBIO DE ESTADO EN PAQUETE CON TRACKING N° " + paquete.getCodTracking();
            String texto = "Se cambio el estado del paquete a " + paquete.getEstadoPaquete();
            sendmail(paquete.getUserOrigen().getEmail(),asunto,texto);
            return new ResponseEntity<>(paqueteRepository.save(_paquete), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @PostMapping("/cargarpaquetes")
    public ResponseEntity<String> uploadCargaPaquete(@RequestParam("file") MultipartFile file, Model model){
        if(file == null) {
            throw new RuntimeException("Debes seleccionar un archivo para cargar");
        }
        String originalname = file.getName();
        String linea;
        String [] valores;
        String trackPaquete, codOrigen, codDestino, stryear, strmes, strdia;
        int id = 1;
        int horigen, morigen;
        java.util.Date fecha = null;
        java.sql.Date fechareal = null;
        if(!file.isEmpty()){
            try{
                InputStream in = file.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                while((linea = br.readLine()) != null){
                    valores = linea.split("-|:");
                    trackPaquete = valores[0];
                    stryear = valores[1].substring(0,4);
                    strmes = valores[1].substring(4,6);
                    strdia = valores[1].substring(6,8);
                    horigen = Integer.parseInt(valores[2]);
                    morigen = Integer.parseInt(valores[3]);
                    codOrigen = trackPaquete.substring(0,4);
                    codDestino = valores[4];
                    Optional<Aeropuerto> aerOrigen = aeropuertoRepository.findByCodAeropuerto(codOrigen);
                    Optional<Aeropuerto> aerDestino = aeropuertoRepository.findByCodAeropuerto(codDestino);
                    java.sql.Timestamp timestamp = java.sql.Timestamp.valueOf(stryear + "-" + strmes + "-" + strdia +
                                                                              " " + valores[2] + ":" + valores[3] + ":00");
                    System.out.println(timestamp.toString());

                    Optional<Usuario> usuario = usuarioRepository.findByEmail("jeanpieres14@gmail.com");
                    Paquete p = new Paquete(usuario.get(), usuario.get(), aerOrigen.get(), aerDestino.get(), trackPaquete, timestamp);
                    paqueteRepository.save(p);
                }
                br.close();
            }catch (IOException ex){
            }
            return new ResponseEntity<String>(originalname, HttpStatus.OK);
        }else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    private void sendmail (String emailto, String asunto, String texto)  {
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        String user = "redexviajes@gmail.com";
        String password = "redex123gg";
        props.put("mail.smtp.starttls.enable","true");
        props.put("mail.smtp.host",host);
        props.put("mail.smtp.user","redexviajes@gmail.com");
        props.put("mail.smtp.password","redex123gg");
        props.put("mail.smtp.port","587");
        props.put("mail.smtp.auth","true");

        Session session =  Session.getDefaultInstance(props);
        MimeMessage msg = new MimeMessage(session);
        try{
            msg.setFrom(new InternetAddress(user));
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(emailto));
            msg.setSubject(asunto);
            msg.setText(texto);
            Transport transport = session.getTransport("smtp");
            transport.connect(host,587,user, password);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
        }
        catch (MessagingException me){
        }
    }


    @GetMapping("/gestionarpaquetesalgoritmo/{param}")
    //public String getAllPaquetesAlgoritmo(@PathVariable("paramdate")Timestamp paramdate) {
    public String getAllPaquetesAlgoritmo(@PathVariable("param") int param) {
        JSONArray data = new JSONArray();
        AntColony solution = new AntColony(2, 120);
        BufferedReader br = null;
        String line = "";
        String [] valores;
        String trackPaquete, codDestino, stryear, strmes, strdia;
        int n=1;
        try {
            br = new BufferedReader(new FileReader("./paquetes.txt"));
            for (int i = 0; i < (param * 500); i++) {
                n++;
                br.readLine();
            }
            while (true) {

                if( (n > 500 * (param+1)) || ((line = br.readLine())==null)) break;
                n++;
                valores = line.split("-|:");
                trackPaquete = valores[0];
                stryear = valores[1].substring(0,4);
                strmes = valores[1].substring(4,6);
                strdia = valores[1].substring(6,8);
                codDestino = valores[4];

                java.sql.Timestamp timestamp = java.sql.Timestamp.valueOf(stryear + "-" + strmes + "-" + strdia +
                        " " + valores[2] + ":" + valores[3] + ":00");
                Date date = new Date(1000);
                date.setTime(timestamp.getTime());
                String datestring = new SimpleDateFormat("yyyyMMdd").format(date);

                Date time = new Date(1000);
                date.setTime(timestamp.getTime());
                String timestring = new SimpleDateFormat("yyyy/MM/dd hh:mm").format(time);
                timestring = timestring.substring(11);

                JSONObject sol = solution.Simulation(trackPaquete,
                        codDestino,
                        timestring,0, datestring);
                data.put(sol);
                solution.resetColony();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println((param+1)* 500);
        return data.toString();
    }

    @GetMapping("/gestionarpaquetesusuario/{id}")
    public List<Paquete> getAllPaquetesbyUser(@PathVariable("id") Long id) {
        List<Paquete> paquetes = new ArrayList<>();
        paquetes = paqueteRepository.findAllByUser(id);
        return paquetes;
    }
}
