package com.springrestbackend.controlador;


import com.springrestbackend.Modelo.Aeropuerto;
import com.springrestbackend.Modelo.Vuelo;
import com.springrestbackend.repo.AeropuertoRepository;
import com.springrestbackend.repo.VueloRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class VueloController {
    @Autowired
    VueloRepository vueloRepository;

    @Autowired
    AeropuertoRepository aeropuertoRepository;

    @PostMapping("/vuelo")
    public Vuelo postVuelo(@RequestBody Vuelo vuelo){
        vueloRepository.save(vuelo);
        return vuelo;
    }

    @GetMapping("/gestionarvuelos")
    public List<Vuelo> getAllVuelos() {
        List<Vuelo> vuelos = new ArrayList<>();
        vueloRepository.findAll().forEach(vuelos::add);
        return vuelos;
    }

    @GetMapping("/gestionarvuelos/{id}")
    public Optional<Vuelo> getVueloById(@PathVariable("id") Long id) {

        Optional<Vuelo> vuelo = vueloRepository.findById(id);
        return vuelo;
    }

    @PutMapping("/gestionarvuelos/{id}")
    public ResponseEntity<Vuelo> updateVuelo(@PathVariable("id") Long id, @RequestBody Vuelo vuelo) {
        Optional<Vuelo> vuelodata = vueloRepository.findById(id);
        if(vuelodata.isPresent()) {
            Vuelo _vuelo = vuelodata.get();
            //_vuelo.setAerOrigen(vuelo.getAerOrigen());
            //_vuelo.setAerDestino(vuelo.getAerDestino());
            //_vuelo.setHoraSalida(vuelo.getHoraSalida());
            //_vuelo.setHoraLlegada(vuelo.getHoraLlegada());
            //_vuelo.setCapacidadActual(vuelo.getCapacidadActual());
            //_vuelo.setCapacidadMaxima(vuelo.getCapacidadMaxima());

            return new ResponseEntity<>(vueloRepository.save(_vuelo), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/eliminarvuelo/{id}")
    public ResponseEntity<String> deleteVuelo(@PathVariable("id") Long id) {
        vueloRepository.deleteById(id);
        //aeropuertoRepository.deleteByCodAeropuerto(codigo);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/cargarvuelos")
    public ResponseEntity<String> uploadCargaVuelo(@RequestParam("file") MultipartFile file, Model model){
        if(file == null) {
            throw new RuntimeException("Debes seleccionar un archivo para cargar");
        }
        String originalname = file.getName();
        String linea;
        String [] valores;
        int id = 1;
        String origen,destino;
        int horaorigen,minorigen,horadestino,mindestino;
        String hourorigen, minuteorigen,hourdestino, minutedestino;
        if(!file.isEmpty()){
            try{
                InputStream in = file.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                while((linea = br.readLine()) != null){
                    valores = linea.split("-|:");
                    origen = valores[0];
                    destino = valores[1];
                    hourorigen = valores[2];
                    minuteorigen = valores[3];
                    hourdestino = valores[4];
                    minutedestino = valores[5];
                    System.out.println(valores[3]);
                    horaorigen = Integer.parseInt(valores[2]);
                    minorigen = Integer.parseInt(valores[3]);
                    horadestino = Integer.parseInt(valores[4]);
                    mindestino = Integer.parseInt(valores[5]);
                    Optional<Aeropuerto> aerorigen = aeropuertoRepository.findByCodAeropuerto(origen);
                    Optional<Aeropuerto> aerdestino = aeropuertoRepository.findByCodAeropuerto(destino);
                    Vuelo vuelo = new Vuelo(aerorigen.get(), aerdestino.get(), horaorigen, horadestino, minorigen, mindestino, 0, 0 ,
                            hourorigen, hourdestino, minuteorigen, minutedestino);
                    vuelo.setCapacidadMaxima(0);
                    vuelo.setCapacidadActual(0);
                    vueloRepository.save(vuelo);
                    //id++;
                }
                br.close();
            }
            catch (IOException ex){
            }
            return new ResponseEntity<String>(originalname, HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
