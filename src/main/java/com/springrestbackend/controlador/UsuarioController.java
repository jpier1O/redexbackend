package com.springrestbackend.controlador;

import com.springrestbackend.Modelo.Usuario;
import com.springrestbackend.payloadcarga.ApiResponse;
import com.springrestbackend.repo.UsuarioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sun.util.resources.cldr.chr.CalendarData_chr_US;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

@RestController
@RequestMapping("/api")
public class UsuarioController {
    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @GetMapping("/gestionarusuarios")
    public List<Usuario> getAllUsuarios() {
        List<Usuario> usuarios = new ArrayList<>();
        usuarioRepository.findAll().forEach(usuarios::add);
        return usuarios;
    }


    @GetMapping("gestionarusuarios/{dni}")
    public Optional<Usuario> getUsuarioByDni(@PathVariable("dni") String dni) {

        Optional<Usuario> aeropuerto = usuarioRepository.findByDni(dni);
        return aeropuerto;
    }

    @GetMapping("obtenerusuario/{id}")
    public Optional<Usuario> getUsuarioByDni(@PathVariable("id") Long id) {

        Optional<Usuario> aeropuerto = usuarioRepository.findById(id);
        return aeropuerto;
    }

    @GetMapping("obtenerrusuarios/{email}")
    public Optional<Usuario> getAeropuertoByEmail(@PathVariable("email") String email) {

          Optional<Usuario> aeropuerto = usuarioRepository.findByEmail(email);
          return aeropuerto;
    }

    @PutMapping("gestionarusuarios/{id}")
    public ResponseEntity<Usuario> updateUsuario(@PathVariable("id") Long id, @RequestBody Usuario usuario) {
        Optional<Usuario> usuariodata = usuarioRepository.findById(id);
        String tokenTest = "";
        if(usuariodata.isPresent()) {
            Usuario _usuario = usuariodata.get();
            _usuario.setDireccion(usuario.getDireccion());
            _usuario.setTelefono(usuario.getTelefono());
            _usuario.setNombres(usuario.getNombres());
            _usuario.setApellidos(usuario.getApellidos());
            return new ResponseEntity<>(usuarioRepository.save(_usuario), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/usuario")
    public ResponseEntity<?>  postUsuario (@RequestBody Usuario usuario) {
        Usuario _usuario = new Usuario(usuario.getNombres(), usuario.getDni(), usuario.getApellidos(), usuario.getEmail(),
                usuario.getPassword(), usuario.getTelefono(), usuario.getDireccion());
        _usuario.setPassword(passwordEncoder.encode("contraseña"));
        _usuario.setRol(usuario.getRol());
        Usuario resultado = usuarioRepository.save(_usuario);

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/usuarios/{email}")
                .buildAndExpand(resultado.getEmail()).toUri();

        String asunto = "CREACION DE CUENTA";
        String texto = "Se realizó creación de cuenta con correo: " + usuario.getEmail() + " y password: contraseña .";

        sendmail(usuario.getEmail(), asunto, texto);
        return ResponseEntity.created(location).body(new ApiResponse(true, "Usuario registrado exitosamente"));
    }

    @PutMapping("/cambiarpassword")
    public ResponseEntity<?> changePassword(@RequestBody Usuario usuario){
        Optional<Usuario> _usuario = usuarioRepository.findByEmail(usuario.getEmail());
        System.out.println(usuario.getResetToken());
        if(usuario.getResetToken().compareTo("Wu5mQ79ps/g6g2wuBRtDv0bKk0whxBSBSqpQeLkKo596lKRcKPvV608eLL5PVUUi") == 0){
            _usuario.get().setPassword(passwordEncoder.encode(usuario.getPassword()));
            return new ResponseEntity<>(usuarioRepository.save(_usuario.get()), HttpStatus.OK);
        }
        return new ResponseEntity(new ApiResponse(false, "No se pudo realizar cambio de contraseña"), HttpStatus.BAD_REQUEST);
    }

    private void sendmail (String emailto, String asunto, String texto)  {
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        String user = "redexviajes@gmail.com";
        String password = "redex123gg";
        props.put("mail.smtp.starttls.enable","true");
        props.put("mail.smtp.host",host);
        props.put("mail.smtp.user","redexviajes@gmail.com");
        props.put("mail.smtp.password","redex123gg");
        props.put("mail.smtp.port","587");
        props.put("mail.smtp.auth","true");

        Session session =  Session.getDefaultInstance(props);
        MimeMessage msg = new MimeMessage(session);
        try{
            msg.setFrom(new InternetAddress(user));
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(emailto));
            msg.setSubject(asunto);
            msg.setText(texto);
            Transport transport = session.getTransport("smtp");
            transport.connect(host,587,user, password);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
        }
        catch (MessagingException me){
        }
    }
}
