package com.springrestbackend.controlador;

import com.springrestbackend.Modelo.Aeropuerto;
import com.springrestbackend.Modelo.Ciudad;
import com.springrestbackend.repo.AeropuertoRepository;
import com.springrestbackend.repo.CiudadRepository;
import org.apache.tomcat.util.http.fileupload.FileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import sun.security.provider.certpath.CertId;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;


//@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class AeropuertoController {

    @Autowired
    AeropuertoRepository aeropuertoRepository;



    @PostMapping("/aeropuerto")
    public Aeropuerto postPaquete (@RequestBody Aeropuerto aeropuerto) {
        Aeropuerto _aeropuerto = aeropuertoRepository.save(new Aeropuerto(aeropuerto.getCodAeropuerto(), aeropuerto.getPais(),
                                                                          aeropuerto.getCiudad(),aeropuerto.getLatitud(),
                                                                          aeropuerto.getLongitud(), aeropuerto.getGmt(),
                                                                          aeropuerto.getContinente(),aeropuerto.getCapacidadActual(),
                                                                          aeropuerto.getCapacidadMaxima()));
        return _aeropuerto;
    }

    @GetMapping("/gestionaraeropuertos")
    public List<Aeropuerto> getAllAeropuertos() {
        List<Aeropuerto> aeropuertos = new ArrayList<>();
        aeropuertoRepository.findAll().forEach(aeropuertos::add);
        return aeropuertos;
    }


    @GetMapping("/gestionaraeropuertos/{id}")
    public Optional<Aeropuerto> getAeropuertoById(@PathVariable("id") Long id) {

        Optional<Aeropuerto> aeropuerto = aeropuertoRepository.findById(id);
        return aeropuerto;
    }

    @GetMapping("/gestionaraeropuertos/continente/{continente}")
    public List<Aeropuerto> getAeropuertoById(@PathVariable("continente") String continente) {
        List<Aeropuerto> aeropuertos = aeropuertoRepository.findByContinente(continente);
        return aeropuertos;
    }

    @PutMapping("/gestionaraeropuertos/{id}")
    public ResponseEntity<Aeropuerto> updateAeropuerto(@PathVariable("id") Long id, @RequestBody Aeropuerto aeropuerto) {
        Optional<Aeropuerto> aeropuertodata = aeropuertoRepository.findById(id);
        if(aeropuertodata.isPresent()) {
            Aeropuerto _aeropuerto = aeropuertodata.get();
            //_aeropuerto.setCodAeropuerto(aeropuerto.getCodAeropuerto());
            //_aeropuerto.setCiudad(aeropuerto.getCiudad());
            //_aeropuerto.setPais(aeropuerto.getPais());
            //_aeropuerto.setLatitud(aeropuerto.getLatitud());
            //_aeropuerto.setLongitud(aeropuerto.getLongitud());
            //_aeropuerto.setGmt(aeropuerto.getGmt());
            //_aeropuerto.setCapacidadActual(aeropuerto.getCapacidadActual());
            //_aeropuerto.setCapacidadMaxima(aeropuerto.getCapacidadMaxima());
            return new ResponseEntity<>(aeropuertoRepository.save(_aeropuerto), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /*@DeleteMapping("gestionaraeropuertos/{id}")
    public ResponseEntity<String> deleteAeropuerto(@PathVariable("id") Long id) {
        aeropuertoRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }*/

    @DeleteMapping("/eliminaraeropuerto/{id}")
    public ResponseEntity<String> deleteAeropuerto(@PathVariable("id") Long id) {
        aeropuertoRepository.deleteById(id);
        //aeropuertoRepository.deleteByCodAeropuerto(codigo);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //private static final Logger logger = Logger.getLogger(AeropuertoController.class.getName());
    //@Re   questMapping(value = "/cargaraeropuertos", method = RequestMethod.POST,)
    @PostMapping("/cargaraeropuertos")
    public ResponseEntity<String> uploadCargaAeropuerto(@RequestParam("file") MultipartFile file, Model model){
        if(file == null) {
            throw new RuntimeException("Debes seleccionar un archivo para cargar");
        }

        String originalname = file.getName();
        //return new ResponseEntity<>("Archivo es cargado exitosamente",HttpStatus.OK);
        String linea;
        String [] valor;
        char tipocontinente;
        String codAeropuerto,nombreCiudad, nombrePais, nombrecorto, continente;
        Float latitud, longitud;
        long id=1;
        int huso;
        if(!file.isEmpty()){
            try{
                InputStream in = file.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                for (int i = 0; i < 10; i++) {
                    linea = br.readLine();
                    valor = linea.split(",");
                    latitud = Float.parseFloat(valor[0]);
                    longitud = Float.parseFloat(valor[1]);
                    codAeropuerto = valor[2];
                    nombreCiudad = valor[3];
                    nombrePais = valor[4];
                    huso = Integer.parseInt(valor[5]);
                    continente = valor[6];
                    Aeropuerto aeropuerto = new Aeropuerto(codAeropuerto,nombreCiudad, nombrePais, latitud, longitud, huso,continente,0,0);
                    aeropuerto.setCapacidadMaxima(0);
                    aeropuerto.setCapacidadActual(0);
                    //aeropuerto.setId(id);
                    aeropuertoRepository.save(aeropuerto);
                    //id++;
                }
                for (int i = 0; i < 30; i++) {
                    linea = br.readLine();
                    valor = linea.split(",");
                    latitud = Float.parseFloat(valor[0]);
                    longitud = Float.parseFloat(valor[1]);
                    codAeropuerto = valor[2];
                    nombreCiudad = valor[3];
                    nombrePais = valor[4];
                    huso = Integer.parseInt(valor[5]);
                    continente = valor[6];
                    Aeropuerto aeropuerto = new Aeropuerto(codAeropuerto,nombreCiudad, nombrePais, latitud, longitud, huso,continente,0,0);
                    aeropuerto.setCapacidadMaxima(0);
                    aeropuerto.setCapacidadActual(0);
                    //aeropuerto.setId(id);
                    aeropuertoRepository.save(aeropuerto);
                    //id++;
                }
                br.close();
            }
            catch(IOException ex){
            }
            return new ResponseEntity<String>(originalname, HttpStatus.OK);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
