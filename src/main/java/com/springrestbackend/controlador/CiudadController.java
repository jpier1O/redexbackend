package com.springrestbackend.controlador;

import com.springrestbackend.Modelo.Ciudad;
import com.springrestbackend.repo.CiudadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


//@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class CiudadController {

    @Autowired
    CiudadRepository ciudadRepository;

    @PostMapping("/ciudad")
    public Ciudad postPaquete(@RequestBody Ciudad ciudad) {
        Ciudad _ciudad = ciudadRepository.save(new Ciudad(ciudad.getNombreCiudad(),
                ciudad.getPais(), ciudad.getContinente()));
        return _ciudad;
    }

    @GetMapping("/gestionarciudades")
    public List<Ciudad> getAllCiudades() {
        List<Ciudad> ciudades = new ArrayList<>();
        ciudadRepository.findAll().forEach(ciudades::add);
        return ciudades;
    }
}
