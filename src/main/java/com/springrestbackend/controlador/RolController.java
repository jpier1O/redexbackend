package com.springrestbackend.controlador;

import com.springrestbackend.Modelo.Rol;
import com.springrestbackend.repo.RolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/")
public class RolController {
    @Autowired
    RolRepository rolRepository;

    @PostMapping("/rol")
    public Rol postPaquete(@RequestBody Rol rol) {
        rolRepository.save(rol);
        return rol;
    }

    @GetMapping("/gestionaroles")
    public List<Rol> getAllRoles() {
        List<Rol> paquetes = new ArrayList<>();
        rolRepository.findAll().forEach(paquetes::add);
        return paquetes;
    }
}
