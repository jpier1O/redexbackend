package com.springrestbackend.controlador;

import com.springrestbackend.Modelo.Rol;
import com.springrestbackend.Modelo.Usuario;
import com.springrestbackend.Seguridad.JwtTokenProvider;
import com.springrestbackend.exception.AppException;
import com.springrestbackend.payloadcarga.*;
import com.springrestbackend.repo.RolRepository;
import com.springrestbackend.repo.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.util.*;


//@CrossOrigin(origins ="http://localhost:8081")
@RestController
@RequestMapping("api/auth")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    RolRepository rolRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.generateToken(authentication);
        Optional<Usuario> usuario = usuarioRepository.findByEmail(loginRequest.getUsernameOrEmail());
        Usuario usuario1 = new Usuario( usuario.get().getNombres(),usuario.get().getDni(), usuario.get().getApellidos(), usuario.get().getEmail(),
                                       usuario.get().getPassword(),usuario.get().getTelefono(),usuario.get().getDireccion());
        usuario1.setId(usuario.get().getId());
        usuario1.setRol(usuario.get().getRol());
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, usuario1));
        //return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }

    @PostMapping("/validate")
    public ResponseEntity<?> validateUser(@Valid @RequestBody SignupRequest signupRequest){
        System.out.println(signupRequest.getEmail());
        if (usuarioRepository.existsByEmail(signupRequest.getEmail())) {
            return new ResponseEntity(new ApiResponse(true, "Email ya tiene cuenta!"), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(new ApiResponse(false, "Email puede ser usado"), HttpStatus.OK);
    }


    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signupRequest) {

        if (usuarioRepository.existsByEmail(signupRequest.getEmail())) {
            return new ResponseEntity(new ApiResponse(false, "Email ya tiene cuenta!"), HttpStatus.BAD_REQUEST);
        }

        Usuario usuario = new Usuario(signupRequest.getNombres(), signupRequest.getDni(), signupRequest.getApellidos(), signupRequest.getEmail(),
                signupRequest.getPassword(), signupRequest.getTelefono(), signupRequest.getDireccion());

        usuario.setPassword(passwordEncoder.encode(usuario.getPassword()));

        //Rol usuariorol =    rolRepository.findByNombre(signupRequest.getRol().getNombre())
        //        .orElseThrow(() -> new AppException("Usuario no tiene rol asignado"));

        Optional<Rol> rol = rolRepository.findByNombre("CLIENTE");
        usuario.setRol(rol.get());


        Usuario resultado = usuarioRepository.save(usuario);

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/usuarios/{email}")
                .buildAndExpand(resultado.getEmail()).toUri();
        String asunto = "CREACION DE CUENTA";
        String texto = "Se realizó creación de cuenta con correo: " + usuario.getEmail() + ".";
        sendmail(usuario.getEmail(), asunto, texto);
        return ResponseEntity.created(location).body(new ApiResponse(true, "Usuario registrado exitosamente"));
    }

    private void sendmail (String emailto, String asunto, String texto)  {
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        String user = "redexviajes@gmail.com";
        String password = "redex123gg";
        props.put("mail.smtp.starttls.enable","true");
        props.put("mail.smtp.host",host);
        props.put("mail.smtp.user","redexviajes@gmail.com");
        props.put("mail.smtp.password","redex123gg");
        props.put("mail.smtp.port","587");
        props.put("mail.smtp.auth","true");

        Session session =  Session.getDefaultInstance(props);
        MimeMessage msg = new MimeMessage(session);
        try{
            msg.setFrom(new InternetAddress(user));
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(emailto));
            msg.setSubject(asunto);
            msg.setText(texto);
            Transport transport = session.getTransport("smtp");
            transport.connect(host,587,user, password);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
        }
        catch (MessagingException me){
        }
    }

    @PostMapping("/resetpassword/{email}")
    public ResponseEntity<?> resetPassword(@PathVariable("email") String email) {
        if (usuarioRepository.existsByEmail(email)) {
            String asunto = "MODIFICACION DE CONTRASEÑA";
            String texto = "Se generó codigo para correo: " +  email + ". Para reestablecer contraseña : Wu5mQ79ps/g6g2wuBRtDv0bKk0whxBSBSqpQeLkKo596lKRcKPvV608eLL5PVUUi" ;
            sendmail(email, asunto, texto);
            return new ResponseEntity<>(new ApiResponse(true,"Se realizó envío a correo"), HttpStatus.OK);
        }
        return new ResponseEntity(new ApiResponse(false, "Email no tiene cuenta!"), HttpStatus.BAD_REQUEST);
    }


}

