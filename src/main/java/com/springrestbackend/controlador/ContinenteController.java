package com.springrestbackend.controlador;

import com.springrestbackend.Modelo.Aeropuerto;
import com.springrestbackend.Modelo.Continente;
import com.springrestbackend.repo.ContinenteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/")
public class ContinenteController {

    @Autowired
    ContinenteRepository continenteRepository;

    @PostMapping("/continente")
    public Continente postContinente (@RequestBody Continente continente) {
        Continente _continente = continenteRepository.save(new Continente(continente.getNombreContinente(), continente.getCodigoContinente()));
        return _continente;
    }

    @GetMapping("/gestionarcontinentes")
    public List<Continente> getAllAeropuertos() {
        List<Continente> continentes = new ArrayList<>();
        continenteRepository.findAll().forEach(continentes::add);
        return continentes;
    }


    @GetMapping("gestionarcontinente/{id}")
    public Optional<Continente> getContinenteById(@PathVariable("id") Long id) {

        Optional<Continente> continente = continenteRepository.findById(id);
        return continente;
    }

    /*@GetMapping("gestionaraeropuertos/continente/{continente}")
    public List<Aeropuerto> getAeropuertoById(@PathVariable("continente") String continente) {
        List<Aeropuerto> aeropuertos = aeropuertoRepository.findByContinente(continente);
        return aeropuertos;
    }*/

    @PutMapping("gestionarcontinentes/{id}")
    public ResponseEntity<Continente> updateContinente(@PathVariable("id") Long id, @RequestBody Continente continente) {
        Optional<Continente> continentedata = continenteRepository.findById(id);
        if(continentedata.isPresent()) {
            Continente _continente = continentedata.get();
            _continente.setNombreContinente(continente.getNombreContinente());
            return new ResponseEntity<>(continenteRepository.save(_continente), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("eliminarcontinente/{codigo}")
    public ResponseEntity<String> deleteContinente(@PathVariable("codigo") String codContinente) {
        continenteRepository.deleteByCodigoContinente(codContinente);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
