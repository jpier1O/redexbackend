package com.springrestbackend.Modelo;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "paquetes")
public class Paquete {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //@NotBlank
    @Size(max = 100)
    @Column(name = "cod_tracking")
    private String codTracking;

    @Column(name = "fecha_origen")
    private Timestamp fechaPedido;

    @Column(name = "fecha_destino")
    private Timestamp fechaDestino;

    @NotNull
    @JoinColumn(name = "id_userorigen", nullable = true, insertable = true, updatable = true)
    @OneToOne
    private Usuario userOrigen;

    @NotNull
    @JoinColumn(name = "id_userdestino",  nullable = true, insertable = true, updatable = true)
    @OneToOne
    private Usuario userDestino;

    @NotNull
    @JoinColumn(name = "id_aerOrigen",  nullable = true, insertable = true, updatable = true)
    @OneToOne
    private Aeropuerto aerOrigen;

    @NotNull
    @JoinColumn(name = "id_aerdestino", nullable = true, insertable = true, updatable = true)
    @OneToOne
    private Aeropuerto aerDestino;

    @Enumerated(EnumType.STRING)
    private EstadoPaqueteName estado;

    @OneToMany
    @JoinColumn(name = "id_paquete", referencedColumnName = "id")
    private List<Viaje> planViaje = new ArrayList<>();

    public Paquete() {

    }

    public Paquete(Usuario userOrigen, Usuario userDestino, Aeropuerto aerOrigen, Aeropuerto aerDestino, String codTracking, Timestamp fechaPedido) {
        this.setUserOrigen(userOrigen);
        this.setUserDestino(userDestino);
        this.setAerOrigen(aerOrigen);
        this.setAerDestino(aerDestino);
        this.setCodTracking(codTracking);
        this.setFechaPedido(fechaPedido);
        this.setEstadoPaquete(EstadoPaqueteName.REGISTRADO);
        //this.setFechaDestino(fechaDestino);
    }

    public String getCodTracking() {
        return codTracking;
    }

    public void setCodTracking(String codTracking) {
        this.codTracking = codTracking;
    }



    public Usuario getUserOrigen() {
        return userOrigen;
    }

    public void setUserOrigen(Usuario userOrigen) {
        this.userOrigen = userOrigen;
    }

    public Usuario getUserDestino() {
        return userDestino;
    }

    public void setUserDestino(Usuario userDestino) {
        this.userDestino = userDestino;
    }


    public Aeropuerto getAerOrigen() {
        return aerOrigen;
    }

    public void setAerOrigen(Aeropuerto aerOrigen) {
        this.aerOrigen = aerOrigen;
    }

    public Aeropuerto getAerDestino() {
        return aerDestino;
    }

    public void setAerDestino(Aeropuerto aerDestino) {
        this.aerDestino = aerDestino;
    }

    public EstadoPaqueteName getEstadoPaquete() {
        return estado;
    }

    public void setEstadoPaquete(EstadoPaqueteName estadoPaquete) {
        this.estado = estadoPaquete;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(Timestamp fechaPedido) {
        this.fechaPedido = fechaPedido;
    }

    public Timestamp getFechaDestino() {
        return fechaDestino;
    }

    public void setFechaDestino(Timestamp fechaDestino) {
        this.fechaDestino = fechaDestino;
    }

    public void addViaje(Viaje viaje) {
        getPlanViaje().add(viaje);
    }

    public void removeViaje(Viaje viaje) {
        getPlanViaje().remove(viaje);
    }

    public List<Viaje> getPlanViaje() {
        return planViaje;
    }

    public void setPlanViaje(List<Viaje> planViaje) {
        this.planViaje = planViaje;
    }
}
