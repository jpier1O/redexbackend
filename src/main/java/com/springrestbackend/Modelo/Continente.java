package com.springrestbackend.Modelo;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "continentes")
public class Continente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 100)
    @Column(name = "nombre_continente")
    private String nombreContinente;

    @NotBlank
    @Size(max = 100)
    @Column(name = "codigo_continente")
    private String codigoContinente;

    public Continente(){

    }

    public Continente(String nombreContinente, String codigoContinente){
        this.setNombreContinente(nombreContinente);
        this.setCodigoContinente(codigoContinente);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreContinente() {
        return nombreContinente;
    }

    public void setNombreContinente(String nombreContinente) {
        this.nombreContinente = nombreContinente;
    }

    public String getCodigoContinente() {
        return codigoContinente;
    }

    public void setCodigoContinente(String codigoContinente) {
        this.codigoContinente = codigoContinente;
    }
}
