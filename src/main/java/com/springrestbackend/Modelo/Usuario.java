package com.springrestbackend.Modelo;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "usuarios")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    @Size(max = 20)
    @Column(name = "DNI")
    private String dni;


    @Size(max = 70)
    @Column(name = "nombres")
    private String nombres;


    @Size(max = 70)
    @Column(name = "apellidos")
    private String apellidos;

    @NotNull
    @Size(max = 80)
    @Column(name = "email")
    private String email;


    @Size(max = 100)
    @Column(name = "password")
    private String password;


    @Column(name = "telefono")
    private String telefono;


    @Column(name = "direccion")
    private String direccion;


    @Column(name = "reset_token")
    private String resetToken;


    //@JoinColumn(name = "id_rol", nullable = true, insertable = true, updatable = true)
    //@OneToOne
    //private Rol rol;
    @ManyToOne
    @JoinColumn(name = "id_rol")
    private Rol rol;

    /*@ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "usuarios_roles",
            joinColumns = @JoinColumn(name = "usuario_id"),
            inverseJoinColumns = @JoinColumn(name = "rol_id"))
    private List<Rol> roles = new ArrayList<>();
*/
    public Usuario(){

    }

    public Usuario(String nombres, String dni, String apellidos, String email, String password, String telefono, String direccion){
        this.setNombres(nombres);
        this.setApellidos(apellidos);
        this.setEmail(email);
        this.setPassword(password);
        this.setTelefono(telefono);
        this.setDireccion(direccion);
        this.setDni(dni);
    }

    public Usuario(String nombres, String dni, String apellidos, String email, String password, String telefono, String direccion, Rol rol){
        this.setNombres(nombres);
        this.setApellidos(apellidos);
        this.setEmail(email);
        this.setPassword(password);
        this.setTelefono(telefono);
        this.setDireccion(direccion);
        this.setDni(dni);
        this.setRol(rol);
    }

    @Override
    public String toString(){
        return "Id  del Usuario es: " + getId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }


    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public String getResetToken() {
        return resetToken;
    }

    public void setResetToken(String resetToken) {
        this.resetToken = resetToken;
    }

    public String getFullNombre() {
        return nombres + " " + apellidos;
    }
}
