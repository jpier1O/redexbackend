package com.springrestbackend.Modelo;

import org.springframework.context.annotation.Configuration;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "aeropuertos")
public class Aeropuerto implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 100)
    @Column(name = "cod_aeropuerto")
    private String codAeropuerto;

    @NotBlank
    @Size(max = 100)
    @Column(name = "Ciudad")
    private String ciudad;


    @NotBlank
    @Size(max = 100)
    @Column(name = "Pais")
    private String pais;

    @Column(name = "latitud")
    private Float latitud;


    @Column(name = "longitud")
    private Float longitud;


    @Column(name = "GMT")
    private Integer gmt;

    @NotBlank
    @Size(max = 100)
    @Column(name = "Continete")
    private String continente;

    @Column(name = "capacidad_actual")
    private int capacidadActual;


    @Column(name = "capacidad_maxima")
    private int capacidadMaxima;


    public Aeropuerto() {
    }

    public Aeropuerto(String codAeropuerto, String ciudad, String pais, Float latitud, Float longitud, Integer gmt, String continente,
                      int capacidadActual, int capacidadMaxima) {
        this.setCodAeropuerto(codAeropuerto);
        this.setCiudad(ciudad);
        this.setPais(pais);
        this.setLatitud(latitud);
        this.setLongitud(longitud);
        this.setGmt(gmt);
        this.setContinente(continente);
        this.setCapacidadActual(capacidadActual);
        this.setCapacidadMaxima(capacidadMaxima);
    }

    public String getCodAeropuerto() {
        return codAeropuerto;
    }

    public void setCodAeropuerto(String codAeropuerto) {
        this.codAeropuerto = codAeropuerto;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Float getLatitud() {
        return latitud;
    }

    public void setLatitud(Float latitud) {
        this.latitud = latitud;
    }

    public Float getLongitud() {
        return longitud;
    }

    public void setLongitud(Float longitud) {
        this.longitud = longitud;
    }

    public Integer getGmt() {
        return gmt;
    }

    public void setGmt(Integer gmt) {
        this.gmt = gmt;
    }

    public String getContinente() {
        return continente;
    }

    public void setContinente(String continente) {
        this.continente = continente;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCapacidadActual() {
        return capacidadActual;
    }

    public void setCapacidadActual(int capacidadActual) {
        this.capacidadActual = capacidadActual;
    }

    public int getCapacidadMaxima() {
        return capacidadMaxima;
    }

    public void setCapacidadMaxima(int capacidadMaxima) {
        this.capacidadMaxima = capacidadMaxima;
    }
}
