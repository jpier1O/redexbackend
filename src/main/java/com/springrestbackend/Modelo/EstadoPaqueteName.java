package com.springrestbackend.Modelo;

public enum EstadoPaqueteName {
    REGISTRADO,
    CAMINO,
    COMPLETADO,
    CANCELADO
}
