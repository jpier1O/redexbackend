package com.springrestbackend.Modelo;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "ciudades")
public class Ciudad {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 100)
    @Column(name = "nombre")
    private String nombreCiudad;

    @NotBlank
    @Size(max = 50)
    @Column(name = "pais")
    private String pais;

    @NotBlank
    @Size(max = 50)
    @Column(name = "continente")
    private String continente;

    public Ciudad() {
    }

    public Ciudad (String nombreCiudad, String pais, String continente) {

        this.setNombreCiudad(nombreCiudad);
        this.setPais(pais);
        this.setContinente(continente);
    }

    public String getNombreCiudad() {
        return nombreCiudad;
    }

    public void setNombreCiudad(String nombreCiudad) {
        this.nombreCiudad = nombreCiudad;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getContinente() {
        return continente;
    }

    public void setContinente(String continente) {
        this.continente = continente;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
