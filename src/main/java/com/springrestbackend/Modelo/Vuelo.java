package com.springrestbackend.Modelo;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "vuelos")
public class Vuelo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //@ManyToOne(fetch = FetchType.LAZY, optional = false)
    //@JoinColumn(name = "id_aero_origen", nullable = false)

    @NotNull
    @JoinColumn(name = "id_aero_origen", nullable = true, insertable = true, updatable = true)
    @OneToOne
    private Aeropuerto aerDestino;

    @NotNull
    @JoinColumn(name = "id_aero_destino", nullable = true, insertable = true, updatable = true)
    @OneToOne
    private Aeropuerto aerOrigen;

    //@ManyToOne(fetch = FetchType.LAZY, optional = false)
    //@JoinColumn(name = "vuelos", nullable = false)

    @NotNull
    @Column(name = "hora_salida")
    private int horaSalida;

    @NotNull
    @Column(name = "hora_llegada")
    private int horaLlegada;

    @NotNull
    @Column(name = "minuto_salida")
    private int minSalida;

    @NotNull
    @Column(name = "minuto_llegada")
    private int minLlegada;


    @NotNull
    @Column(name = "hour_salida")
    private String hourSalida;

    @NotNull
    @Column(name = "hour_llegada")
    private String hourLlegada;

    @NotNull
    @Column(name = "minute_salida")
    private String minuteSalida;

    @NotNull
    @Column(name = "minute_llegada")
    private String minuteLlegada;


    @Column(name = "capacidad_actual")
    private int capacidadActual;


    @Column(name = "capacidad_maxima")
    private int capacidadMaxima;

    /*
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name="vuelo_x_paquete", joinColumns = @JoinColumn(name = "id_vuelo"),
            inverseJoinColumns = @JoinColumn(name = "id_paquete"))
    private List<Paquete> paquetes = new ArrayList<>();
    */

    public Vuelo() {
    }

    public Vuelo(Aeropuerto aerOrigen, Aeropuerto aerDestino,int horaSalida, int horaLlegada, int minSalida, int minLlegada,
                 int capacidadActual, int capacidadMaxima ){
        this.setAerOrigen(aerOrigen);
        this.setAerDestino(aerDestino);
        this.setHoraSalida(horaSalida);
        this.setHoraLlegada(horaLlegada);
        this.setMinSalida(minSalida);
        this.setMinLlegada(minLlegada);
        this.setCapacidadActual(capacidadActual);
        this.setCapacidadMaxima(capacidadMaxima);
    }

    public Vuelo(Aeropuerto aerOrigen, Aeropuerto aerDestino,int horaSalida, int horaLlegada, int minSalida, int minLlegada,
                 int capacidadActual, int capacidadMaxima, String hourLlegada, String hourSalida, String minuteLlegada,
                 String minuteSalida){
        this.setAerOrigen(aerOrigen);
        this.setAerDestino(aerDestino);
        this.setHoraSalida(horaSalida);
        this.setHoraLlegada(horaLlegada);
        this.setMinSalida(minSalida);
        this.setMinLlegada(minLlegada);
        this.setCapacidadActual(capacidadActual);
        this.setCapacidadMaxima(capacidadMaxima);
        this.setHourSalida(hourSalida);
        this.setHourLlegada(hourLlegada);
        this.setMinuteSalida(minuteSalida);
        this.setMinuteLlegada(minuteLlegada);
    }

    public Aeropuerto getAerOrigen() {
        return aerOrigen;
    }

    public void setAerOrigen(Aeropuerto aerOrigen) {
        this.aerOrigen = aerOrigen;
    }

    public Aeropuerto getAerDestino() {
        return aerDestino;
    }

    public void setAerDestino(Aeropuerto aerDestino) {
        this.aerDestino = aerDestino;
    }


    public int getCapacidadActual() {
        return capacidadActual;
    }

    public void setCapacidadActual(int capacidadActual) {
        this.capacidadActual = capacidadActual;
    }

    public int getCapacidadMaxima() {
        return capacidadMaxima;
    }

    public void setCapacidadMaxima(int capacidadMaxima) {
        this.capacidadMaxima = capacidadMaxima;
    }

    public int getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(int horaSalida) {
        this.horaSalida = horaSalida;
    }

    public int getHoraLlegada() {
        return horaLlegada;
    }

    public void setHoraLlegada(int horaLlegada) {
        this.horaLlegada = horaLlegada;
    }

    public int getMinSalida() {
        return minSalida;
    }

    public void setMinSalida(int minSalida) {
        this.minSalida = minSalida;
    }

    public int getMinLlegada() {
        return minLlegada;
    }

    public void setMinLlegada(int minLlegada) {
        this.minLlegada = minLlegada;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHourSalida() {
        return hourSalida;
    }

    public void setHourSalida(String hourSalida) {
        this.hourSalida = hourSalida;
    }

    public String getHourLlegada() {
        return hourLlegada;
    }

    public void setHourLlegada(String hourLlegada) {
        this.hourLlegada = hourLlegada;
    }

    public String getMinuteSalida() {
        return minuteSalida;
    }

    public void setMinuteSalida(String minuteSalida) {
        this.minuteSalida = minuteSalida;
    }

    public String getMinuteLlegada() {
        return minuteLlegada;
    }

    public void setMinuteLlegada(String minuteLlegada) {
        this.minuteLlegada = minuteLlegada;
    }
}
