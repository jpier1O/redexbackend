package com.springrestbackend.Modelo;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Entity
@Table(name = "viajes")
public class Viaje {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @JoinColumn(name = "id_aero_origen", nullable = true, insertable = true, updatable = true)
    @OneToOne
    private Aeropuerto aerDestino;

    @NotNull
    @JoinColumn(name = "id_aero_destino", nullable = true, insertable = true, updatable = true)
    @OneToOne
    private Aeropuerto aerOrigen;

    @Column(name = "fecha_origen")
    private Timestamp fechaOrigen;

    @Column(name = "fecha_destino")
    private Timestamp fechaDestino;

    @Column(name = "hora_origen")
    private String horaOrigen;

    @Column(name = "hora_destino")
    private String horaDestino;

    @Column(name = "id_paquete")
    private Long idPaquete;

    public Viaje() {

    }

    public Viaje(Aeropuerto aerOrigen, Aeropuerto aerDestino, Timestamp fechaOrigen, Timestamp fechaDestino) {
        this.setAerOrigen(aerOrigen);
        this.setAerDestino(aerDestino);
        this.setFechaOrigen(fechaOrigen);
        this.setFechaDestino(fechaDestino);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Aeropuerto getAerDestino() {
        return aerDestino;
    }

    public void setAerDestino(Aeropuerto aerDestino) {
        this.aerDestino = aerDestino;
    }

    public Aeropuerto getAerOrigen() {
        return aerOrigen;
    }

    public void setAerOrigen(Aeropuerto aerOrigen) {
        this.aerOrigen = aerOrigen;
    }

    public Timestamp getFechaOrigen() {
        return fechaOrigen;
    }

    public void setFechaOrigen(Timestamp fechaOrigen) {
        this.fechaOrigen = fechaOrigen;
    }

    public Timestamp getFechaDestino() {
        return fechaDestino;
    }

    public void setFechaDestino(Timestamp fechaDestino) {
        this.fechaDestino = fechaDestino;
    }

    public String getHoraOrigen() {
        return horaOrigen;
    }

    public void setHoraOrigen(String horaOrigen) {
        this.horaOrigen = horaOrigen;
    }

    public String getHoraDestino() {
        return horaDestino;
    }

    public void setHoraDestino(String horaDestino) {
        this.horaDestino = horaDestino;
    }
}
