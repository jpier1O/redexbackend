package com.springrestbackend.payloadcarga;

import javax.validation.constraints.NotBlank;

public class PasswordRequest {
    @NotBlank
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
