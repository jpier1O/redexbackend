package com.springrestbackend.payloadcarga;

import com.springrestbackend.Modelo.Rol;
import com.springrestbackend.Modelo.RolName;
import com.springrestbackend.Modelo.Usuario;
import com.springrestbackend.repo.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

public class JwtAuthenticationResponse {
    private String accessToken;
    private String tokenType = "Bearer";
    private String nombres;
    private Long id;
    private String rol;


    @Autowired
    UsuarioRepository usuarioRepository;

    public JwtAuthenticationResponse(String accessToken) {

        this.setAccessToken(accessToken);

    }

    public JwtAuthenticationResponse(String accessToken, Usuario usuario) {
        String email = usuario.getEmail();
        this.setNombres(usuario.getNombres());
        this.setId(usuario.getId());
        this.setRol(usuario.getRol().getNombre());
        this.setAccessToken(accessToken);
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }
}
