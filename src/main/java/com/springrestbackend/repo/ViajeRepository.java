package com.springrestbackend.repo;

import com.springrestbackend.Modelo.Viaje;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ViajeRepository extends JpaRepository<Viaje, Long> {

}
