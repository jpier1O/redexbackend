package com.springrestbackend.repo;

import com.springrestbackend.Modelo.Paquete;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface PaqueteRepository extends JpaRepository<Paquete, Long> {

    //Optional<Paquete> findByTracking(String codTracking);


    @Query(value = "SELECT COUNT(*) FROM paquetes p", nativeQuery = true)
    public Long findCount ();

    @Query(value = "SELECT MAX(id) FROM paquetes p", nativeQuery = true)
    public Long findMax ();

    //@Query(value = "select * from paquetes p where p.fecha_origen <= :param AND p.estado=:estado", nativeQuery = true)
    //List<Paquete> findAllTimeBefore(@Param("param") Timestamp paramdate, @Param("estado") String estado);
    //Boolean existByTracking(String codTracking);

    @Query(value = "select * from paquetes p where  p.estado=:estado", nativeQuery = true)
    ArrayList<Paquete> findAllTimeBefore(@Param("estado") String estado);
    //List<Paquete> findBy(Sort sort);

    @Query(value = "select * from paquetes p where  p.id_userorigen=:id", nativeQuery = true)
    ArrayList<Paquete> findAllByUser(@Param("id") Long id);
}
