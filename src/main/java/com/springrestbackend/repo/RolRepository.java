package com.springrestbackend.repo;

import com.springrestbackend.Modelo.Rol;
import com.springrestbackend.Modelo.RolName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RolRepository extends JpaRepository<Rol, Long> {
    Optional<Rol> findByNombre(String nombre);


}
