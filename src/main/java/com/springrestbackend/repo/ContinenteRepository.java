package com.springrestbackend.repo;

import com.springrestbackend.Modelo.Continente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;


public interface ContinenteRepository extends JpaRepository<Continente, Long> {

    @Transactional
    public void deleteByCodigoContinente (String codigoContinente);
}
