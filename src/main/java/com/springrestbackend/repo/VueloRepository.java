package com.springrestbackend.repo;

import com.springrestbackend.Modelo.Vuelo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VueloRepository extends JpaRepository<Vuelo, Long> {


}
