package com.springrestbackend.repo;

import com.springrestbackend.Modelo.Almacen;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlmacenRepository extends JpaRepository<Almacen, Long> {
}
