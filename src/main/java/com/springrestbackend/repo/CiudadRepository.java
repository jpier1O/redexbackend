package com.springrestbackend.repo;

import com.springrestbackend.Modelo.Ciudad;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CiudadRepository extends JpaRepository<Ciudad, Long> {
    //Optional<Ciudad> findById(Long Id);

    //@Query("SELECT c FROM CIUDADES c WHERE v.continente = :continente")
    //List<Ciudad> findByContinente(@Param("continente") String continente, Sort sort);

}
