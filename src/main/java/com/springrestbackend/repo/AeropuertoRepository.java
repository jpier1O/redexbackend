package com.springrestbackend.repo;

import com.springrestbackend.Modelo.Aeropuerto;
import com.springrestbackend.Modelo.Ciudad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


public interface AeropuertoRepository extends JpaRepository<Aeropuerto, Long> {
    @Query(value = "SELECT a FROM aeropuertos a WHERE a.continente = ?1", nativeQuery = true)
    public List<Aeropuerto> findByContinente (String continent);

    Optional<Aeropuerto> findByCodAeropuerto(String codAeropuerto);

    //@Transactional
    //public void deleteByCodAeropuerto (String codAeropuerto);

}
