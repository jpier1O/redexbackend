package com.springrestbackend.Seguridad;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.springrestbackend.Modelo.Usuario;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class UserPrincipal implements UserDetails {

    private Long id;

    private String dni;

    private String nombres;

    private String apellidos;

    private String email;


    @JsonIgnore
    private String password;

    private String telefono;

    private String direccion;

    private Collection<? extends GrantedAuthority> authorities;

    public UserPrincipal(Long id, String dni, String nombres, String apellidos, String email, String password,
                         String telefono, String direccion){//}, Collection<? extends GrantedAuthority> authorities){
        this.id = id;
        this.dni = dni;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.email = email;
        this.password = password;
        this.telefono = telefono;
        this.direccion = direccion;
        //this.authorities = authorities;
    }

    public static UserPrincipal create(Usuario usuario) {

        //List<GrantedAuthority> authorities = usuario.getRoles().stream().map(rol ->
        //        new SimpleGrantedAuthority(rol.getName().name())).collect(Collectors.toList());

        return new UserPrincipal(
                usuario.getId(),
                usuario.getDni(),
                usuario.getNombres(),
                usuario.getApellidos(),
                usuario.getEmail(),
                usuario.getPassword(),
                usuario.getTelefono(),
                usuario.getDireccion()//,
                //authorities
        );
    }


    public Long getId() {
        return id;
    }


    public String getNombres() {
        return nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired(){
        return true;
    }

    @Override
    public boolean equals( Object o) {
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;
        UserPrincipal that = (UserPrincipal) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String getDni() {
        return dni;
    }
}
