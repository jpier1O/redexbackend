package com.springrestbackend.algorithm;
import org.json.JSONArray;
import org.json.JSONObject;
import sun.nio.ch.Net;

import java.util.*;

public class AntColony {

    private int iter; //numero de iteraciones
    private int nAnts; //numero de hormigas
    private Node origin;
    private Node destination;
    private ArrayList<Ant> ants; //arreglo de nAnts hormigas
    private Network network; //colonia con nodos y caminos entre nodos
    //private HashMap<Path, Double> pheromonePerPath; //objeto que anade feromonas a los caminos entre nodos
    private int diasim;



    public AntColony(int iter, int nAnts){

        this.iter = iter;
        this.nAnts = nAnts;

        network = new Network();
        this.network.generateNetwork();
//        System.out.println("Mundo generado");
        this.network.setProbs();
//        System.out.println("probabilidades seteadas");
        this.diasim = 0;


        //if(this.origin==null || this.destination==null) return;



    }


    /*Metodo para crear las hormigas pertenecientes a la colonia*/
    private void GenerateAnts(){

        for(int i = 0; i < nAnts; i++){
            Ant a = new Ant(this.origin);
            this.ants.add(a);
        }
    }


    public JSONObject Simulation(String origin, String destination, String h, int diasim, String diavuelo){
        if(diasim != this.diasim){
            this.diasim = diasim;
            reducircapacidad();
        }

        this.origin = network.getNode(origin.substring(0,4)); // se setea el nodo inicial
        this.destination = network.getNode(destination); // se setea el destino

        /*ArrayList<Path> aux = this.network.getPaths();
        for(Path p : aux){
            if(p.getNi().getName().equals(origin) && p.getNf().getName().equals(destination)){
                p.setProbability(0.2);
            }
        }*/

        ants = new ArrayList<>();
        GenerateAnts();



        for(int i = 0; i<this.iter; i++){

            ArrayList<Thread> threadpool = new ArrayList<>();
            int cont = 0;

            int partitionSize = this.ants.size() / 8;
            List<List<Ant>> partitions = new LinkedList<List<Ant>>();
            for (int k = 0; k < ants.size(); k += partitionSize) {
                partitions.add(ants.subList(k,
                        Math.min(k + partitionSize, ants.size() )));
            }
            for(List<Ant> a : partitions){

                Thread antThread = new Thread(new Anttravel(a, this.network, this.destination), "ant " + cont++);
                antThread.start();
                threadpool.add(antThread);



            }

            for(Thread t : threadpool){
                try {
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


            HashMap<Path, Double> pheromonePerPath = new HashMap<>();

            double shortestTime = Double.MAX_VALUE;
            Ant fastestAnt = null;

            for(Ant ant : this.ants){
                double time = ant.getTtraveled();
                if(time < shortestTime){
                    fastestAnt = ant;
                    shortestTime = time;
                }

                for(Path path : ant.getTraveledPaths()){
                    if(pheromonePerPath.containsKey(path)){
                        pheromonePerPath.put(path, pheromonePerPath.get(path) + 1.0/time);
                    }else{
                        pheromonePerPath.put(path, 1.0/time);
                    }

                }


            }

            for(Path path : fastestAnt.getTraveledPaths()){
                pheromonePerPath.put(path, pheromonePerPath.get(path)+ 10.0/shortestTime );
            }


            for(Path path : this.network.getPaths()){
                if(pheromonePerPath.containsKey(path)){
                    pheromonePerPath.put(path, pheromonePerPath.get(path) * 0.7);
                } else {
                    pheromonePerPath.put(path, 0.0);
                }
            }

            this.network.updateProbs(pheromonePerPath);

            resetAnts();


        }

        ArrayList<Node> shortestpath = new ArrayList<>();
        ArrayList<Path> followedpath = new ArrayList<>();
        shortestpath.add(this.origin);
        Node current = this.origin;
        double finaltime = 0.0;
        Node prev = null;

        while(current != this.destination){
            //System.out.println(current.getName());
            Path mostprob = current.mostProbablePath();
            if(shortestpath.contains(mostprob.getNf())){
                mostprob.setProbability(0.0);
                mostprob = current.mostProbablePath();
            }
            followedpath.add(mostprob);
            finaltime += mostprob.getTime();
            prev = current;
            current = current.mostProbablePath().getNf();
            shortestpath.add(current);
        }

        JSONObject ret = new JSONObject();

        JSONArray plan = new JSONArray();
        for(Path p :followedpath){
            p.setCapacidad(p.getCapacidad()-1);
            //System.out.println(p.getNi().getName()+" "+p.getNf().getName()+" "+p.getHi()+"-"+p.getHf());
            JSONObject objplan = new JSONObject();
            objplan.put("ini", p.getNi().getName() );
            objplan.put("fin", p.getNf().getName());
            objplan.put("hi", p.getHi());
            objplan.put("hf", p.getHf());
            objplan.put("tiempovuelo", p.getTime());
            plan.put(objplan);

        }

        ret.put("Plan", plan);
        ret.put("Origen", this.origin.getCiudad());
        ret.put("Destino", this.destination.getCiudad());

        String hi = followedpath.get(0).getHi();
        double wait = network.flyTime(h, hi, 0, 0);

        finaltime += wait;

        //System.out.println(wait + " " + h);


        int conti = this.origin.getCont().equals(this.destination.getCont()) ? 1 : 0;
        double th = 0;
        if(conti == 1){
            th = 24.0;
        } else{
            th = 48.0;
        }

        if((finaltime/60) <= th){
            //System.out.println("Se puede realizar el envio - tiempo: " + finaltime/60 + " / " + finaltime );
            ret.put("Detalle", "Se puede realizar el envio - tiempo: " + finaltime/60 + " / " + finaltime);
            ret.put("Estado", "En transito");
            ret.put("Atendido", 1);

        } else {
            //System.out.println("No se puede realizar el envio - tiempo: " + finaltime/60+ " / " + finaltime );
            ret.put("Detalle", "No se puede realizar el envio - tiempo: " + finaltime/60+ " / " + finaltime);
            ret.put("Estado", "No atendido");
            ret.put("Atendido", 0);
        }

        //System.out.println("--------------------");

        ret.put("Diavuelo", diavuelo);
        ret.put("Numero", origin);
        ret.put("Ttotal",finaltime);
        return ret;

    }

    private void resetAnts(){
        for(Ant a : this.ants){
            a.setCurrent(this.origin);
            a.clearAnt();
        }
    }

    private Path getProbablePath(Node n){
        Random random = new Random();
        int stop = 20;

        while(stop>0){
            double r = random.nextDouble();
            double counter = 0.0;
            for(Path path : n.getPaths()){
                counter += path.getProbability();
                if(r < counter){
                    return path;
                }

            }
            stop--;
        }
        return n.getPaths().get(0);

    }

    public void resetColony(){
        this.network.setProbs();
        this.resetAnts();

    }

    private void reducircapacidad(){
        network.resetflights();
    }

    class Anttravel implements Runnable {
        List<Ant> al;
        Network network;
        Node destination;

        Anttravel(List<Ant> al, Network network, Node destination) { this.al = al; this.network = network; this.destination = destination; }
        public void run() {
            for(Ant a : this.al){
                Path prevp = null;
                while(!a.getCurrent().equals(this.destination)){
                    Node currnode = a.getCurrent();
                    Path selectedPath = getProbablePath(currnode);

                    double layover = 0.0;
                    if(prevp != null){
                        layover = this.network.flyTime(prevp.getHf(), selectedPath.getHi(), 0, 0);
                    }
                    a.setCurrent(selectedPath.getNf());
                    a.addTraveledPath(selectedPath);
                    a.addTtraveled(layover);
                    prevp = selectedPath;
                }
            }

        }
    }


}
