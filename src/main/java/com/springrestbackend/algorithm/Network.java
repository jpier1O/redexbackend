package com.springrestbackend.algorithm;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static java.lang.Math.abs;

public class Network {

    private ArrayList<Node> nodes;
    private ArrayList<Path> paths;


    public Network(){
        this.nodes = new ArrayList<>();
        this.paths = new ArrayList<>();
    }

    public void AddNode(Node n){
        this.nodes.add(n);
    }

    public void AddPath(Path p){
        this.paths.add(p);
    }

    public ArrayList<Node> getNodes(){
        return this.nodes;
    }

    public ArrayList<Path> getPaths(){
        return this.paths;
    }

    public Node getNode(String name){
        for(Node n : this.nodes){
            if(n.getName().equals(name)) return n;
        }

        return null;
    }

    public void generateNetwork(){


        CSVReader reader = new CSVReader();

        this.nodes = reader.readCities("./aeropuertos.csv");
        ArrayList<String[]> paths = reader.readPaths("./planes_vuelo.txt");

        //System.out.println("Termine de leer");
        //System.out.println(this.nodes.size());
        //System.out.println(paths.size());

        for(String[] p : paths){
            //obtengo los nodos para ese camino
            Node n1 = this.getNode(p[0]);
            Node n2 = this.getNode(p[1]);
            // genero el tiempo necesario de vuelo considerando huso horario

            double time = this.flyTime(p[2], p[3], n1.getGmt(), n2.getGmt());
            //

            Path npath = new Path(p[2], p[3], time, n1, n2);

            this.paths.add(npath);
            n1.addPath(npath);
        }

        setProbs();

    }

    public double flyTime(String h1, String h2, int u1, int u2){

        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm dd/MM/YYYY");
        Date t1 = null;
        Date t2 = null;
        try {
            t1 = formatter.parse(h1 + " 01/01/2000");
            t2 = formatter.parse(h2 + " 01/01/2000");
        } catch (ParseException e){

        }

        Calendar salida = Calendar.getInstance();
        Calendar llegada = Calendar.getInstance();
        salida.setTime(t1);
        llegada.setTime(t2);


        if(t1.after(t2) || t1.equals(t2)){
            llegada.add(Calendar.DATE, 1);
        }

        t1 = salida.getTime();
        t2 = llegada.getTime();

        long aux1 = TimeUnit.MILLISECONDS.toMinutes(t1.getTime()) - (u1*60);
        long aux2 = TimeUnit.MILLISECONDS.toMinutes(t2.getTime()) - (u2*60);

        long ret = aux2 - aux1;

        return ret;
    }

    public void setProbs(){
        for(Node n : this.nodes){
            ArrayList<Path> ps = n.getPaths();
            for(Path p : ps){
                p.setProbability(1.0/ps.size());
            }
        }
    }

    public void updateProbs(HashMap<Path, Double> pher){

        for(Node n : this.nodes){
            ArrayList<Path> pat = n.getPaths();
            double total = 0.0;
            for(Path p : pat){
                if(pher.get(p) != null){
                    total += pher.get(p);
                }
            }

            for(Path p : pat){
                if(pher.get(p) != null){
                    double newprob = (p.getProbability() * pher.get(p))/total;
                    if(p.getCapacidad() == 0) p.setProbability(0.0);
                    else p.setProbability(newprob);
                }
            }

        }
    }

    public void resetflights(){
        for(Path p : paths){
            p.setCapacidad(100);
        }
    }




}
