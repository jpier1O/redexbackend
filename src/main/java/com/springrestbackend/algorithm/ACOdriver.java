package com.springrestbackend.algorithm;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

class ACOdriver {

    public static void main (String[] args) throws java.lang.Exception
    {
        AntColony solution = new AntColony(2, 200);


        CSVReader r = new CSVReader();
        ArrayList<String[]> parcels = r.readParcels("/home/jose/Downloads/parcels.txt");
        //ArrayList<String[]> parcels = r.readParcels(args[0]);

        System.out.println("Lectura de parcelas completa");

        JSONArray data = new JSONArray();
        int diaini = Integer.parseInt(parcels.get(0)[1]);

        for(String[] p : parcels){
            //if(p[3].equals("LSZB")) continue;
            System.out.println(p[0]);
            JSONObject sol = solution.Simulation(p[0], p[3], p[2], Integer.parseInt(p[1]) - diaini, p[1]);

            data.put(sol);
            solution.resetColony();

        }

        File f = new File("/home/jose/Downloads/out.json");
        //File f = new File(args[1]);
        f.createNewFile();
        FileWriter fooWriter = new FileWriter(f, false);
        fooWriter.write(data.toString());
        fooWriter.close();

        int contv = 0;
        int contnv = 0;

        for (int i = 0; i < data.length(); i++) {
            JSONObject o = data.getJSONObject(i);
            if(o.getInt("Atendido") == 1) contv++;
            else contnv++;

        }

        System.out.println("Atendidos: " + contv);
        System.out.println("No atendidos: " + contnv);
    }
}