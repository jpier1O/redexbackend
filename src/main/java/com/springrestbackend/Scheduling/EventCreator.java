package com.springrestbackend.Scheduling;

import com.springrestbackend.Modelo.Aeropuerto;
import com.springrestbackend.Modelo.EstadoPaqueteName;
import com.springrestbackend.Modelo.Paquete;
import com.springrestbackend.Modelo.Viaje;
import com.springrestbackend.algorithm.AntColony;
import com.springrestbackend.repo.AeropuertoRepository;
import com.springrestbackend.repo.PaqueteRepository;
import com.springrestbackend.repo.ViajeRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class EventCreator {


    @Autowired
    PaqueteRepository paqueteRepository;

    @Autowired
    AeropuertoRepository aeropuertoRepository;

    @Autowired
    ViajeRepository viajeRepository;

    @Scheduled(fixedRate = 600000, initialDelay = 10000)
    public void finpaquetemanual(){
        ArrayList<Paquete> paquetes = new ArrayList<>();
        paquetes = paqueteRepository.findAllTimeBefore("CAMINO");
        for (int i = 0; i < paquetes.size(); i++) {
            java.sql.Timestamp fechadestino = paquetes.get(i).getFechaDestino();
            long time = System.currentTimeMillis();
            java.sql.Timestamp fechanow = new java.sql.Timestamp(time);

            if(fechanow.after(fechadestino)){
                paquetes.get(i).setEstadoPaquete(EstadoPaqueteName.COMPLETADO);
                paqueteRepository.save(paquetes.get(i));
                String asunto = "CAMBIO DE ESTADO EN PAQUETE CON TRACKING N° " + paquetes.get(i).getCodTracking();
                String texto = "Se cambio el estado del paquete a " + paquetes.get(i).getEstadoPaquete();
                sendmail(paquetes.get(i).getUserOrigen().getEmail(),asunto,texto);
            }
        }
    }



    @Scheduled(fixedRate = 1200000, initialDelay = 10000)
    public void algorithmanual(){
        ArrayList<Paquete> paquetes = new ArrayList<>();
        paquetes = paqueteRepository.findAllTimeBefore("REGISTRADO");
        JSONArray data = new JSONArray();
        AntColony solution = new AntColony(2, 180);
        for (int i = 0; i < paquetes.size(); i++) {
            java.sql.Timestamp timestamp = paquetes.get(i).getFechaPedido();
            Date date = new Date(1000);
            date.setTime(timestamp.getTime());
            String datestring = new SimpleDateFormat("yyyyMMdd").format(date);

            Date time = new Date(1000);
            date.setTime(timestamp.getTime());
            String timestring = new SimpleDateFormat("yyyy/MM/dd hh:mm").format(time);
            timestring = timestring.substring(11);

            JSONObject sol = solution.Simulation(paquetes.get(i).getCodTracking(),
                    paquetes.get(i).getAerDestino().getCodAeropuerto(),
                    timestring,0, datestring);
            String resultado = sol.toString();
            ArrayList<Viaje> planviaje = new ArrayList<>();
            if(resultado.contains("Se puede realizar el envio")){
                JSONArray plan = (JSONArray) sol.get("Plan");
                String fechaini = (String) sol.get("Diavuelo");
                for (int j = 0; j < plan.length(); j+=5) {
                    Viaje viaje = new Viaje();
                    JSONObject auxviaje = (JSONObject) plan.get(j);
                    String origen = (String) auxviaje.get("ini");
                    Optional<Aeropuerto> auxaerori = aeropuertoRepository.findByCodAeropuerto(origen);
                    String destino = (String) auxviaje.get("fin");
                    Optional<Aeropuerto> auxaerdes = aeropuertoRepository.findByCodAeropuerto(destino);
                    String horaviaje = (String) auxviaje.get("hi");
                    java.sql.Timestamp datetravelini = java.sql.Timestamp.valueOf(fechaini.substring(0,4) +
                            "-" + fechaini.substring(4,6) + "-" + fechaini.substring(6,8) +
                            " " + horaviaje.substring(0,2) + ":" + horaviaje.substring(3,5) + ":00");
                    Double tiempovuelo = (Double) auxviaje.get("tiempovuelo");

                    long longdatetravefin = datetravelini.getTime() + tiempovuelo.longValue() * 60 * 1000;
                    java.sql.Timestamp datetravelfin = new java.sql.Timestamp(longdatetravefin);

                    viaje.setAerOrigen(auxaerori.get());
                    viaje.setAerDestino(auxaerdes.get());
                    viaje.setFechaOrigen(datetravelini);
                    viaje.setFechaDestino(datetravelfin);
                    viaje.setHoraOrigen(datetravelini.toString().substring(11,16));
                    viaje.setHoraDestino(datetravelfin.toString().substring(11,16));
                    viajeRepository.save(viaje);
                    planviaje.add(viaje);
                }
                paquetes.get(i).setPlanViaje(planviaje);

                Double d = (Double)sol.get("Ttotal");
                int tiempototal = d.intValue();

                Timestamp auxtime = new Timestamp(System.currentTimeMillis());
                auxtime.setTime(paquetes.get(i).getFechaPedido().getTime() + ((tiempototal*60) * 1000));
                paquetes.get(i).setFechaDestino(auxtime);

                paquetes.get(i).setEstadoPaquete(EstadoPaqueteName.CAMINO);
                paqueteRepository.save(paquetes.get(i));
                String asunto = "CAMBIO DE ESTADO EN PAQUETE CON TRACKING N° " + paquetes.get(i).getCodTracking();
                String newline = System.getProperty("line.separator");
                String texto = "Se cambio el estado del paquete a " + paquetes.get(i).getEstadoPaquete() +
                                newline + "Plan de viaje:" ;
                String planvuelo = "";
                for (int k = 0; k < paquetes.get(i).getPlanViaje().size(); k++) {
                    List<Viaje> auxplanviaje = paquetes.get(i).getPlanViaje();
                    planvuelo = planvuelo + newline + "Origen: " + auxplanviaje.get(k).getAerOrigen().getCiudad() + ", " +
                                auxplanviaje.get(k).getFechaOrigen().toString()+ newline +
                                "Destino: " + auxplanviaje.get(k).getAerDestino().getCiudad() + ", " +
                                auxplanviaje.get(k).getFechaDestino().toString() + newline;
                }
                texto = texto + planvuelo + newline  + "Gracias por disfrutar de la experiencia de Redex";
                sendmail(paquetes.get(i).getUserOrigen().getEmail(),asunto,texto);
            }
            solution.resetColony();
        }
    }

    private void sendmail (String emailto, String asunto, String texto)  {
        Properties props = System.getProperties();
        String host = "smtp.gmail.com";
        String user = "redexviajes@gmail.com";
        String password = "redex123gg";
        props.put("mail.smtp.starttls.enable","true");
        props.put("mail.smtp.host",host);
        props.put("mail.smtp.user","redexviajes@gmail.com");
        props.put("mail.smtp.password","redex123gg");
        props.put("mail.smtp.port","587");
        props.put("mail.smtp.auth","true");

        Session session =  Session.getDefaultInstance(props);
        MimeMessage msg = new MimeMessage(session);
        try{
            msg.setFrom(new InternetAddress(user));
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(emailto));
            msg.setSubject(asunto);
            msg.setText(texto);
            Transport transport = session.getTransport("smtp");
            transport.connect(host,587,user, password);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();
        }
        catch (MessagingException me){
        }
    }
}
